	████████╗███████╗████████╗██████╗ ██╗███████╗    ██████╗  ██████╗  ██████╗  ██████╗ 
	╚══██╔══╝██╔════╝╚══██╔══╝██╔══██╗██║██╔════╝    ╚════██╗██╔═████╗██╔═████╗██╔═████╗
	   ██║   █████╗     ██║   ██████╔╝██║███████╗     █████╔╝██║██╔██║██║██╔██║██║██╔██║
	   ██║   ██╔══╝     ██║   ██╔══██╗██║╚════██║     ╚═══██╗████╔╝██║████╔╝██║████╔╝██║
	   ██║   ███████╗   ██║   ██║  ██║██║███████║    ██████╔╝╚██████╔╝╚██████╔╝╚██████╔╝
	   ╚═╝   ╚══════╝   ╚═╝   ╚═╝  ╚═╝╚═╝╚══════╝    ╚═════╝  ╚═════╝  ╚═════╝  ╚═════╝ 
                                                                                    

Gra w Ramach projektu przedmiotu MZMO.

Opis gry:
Jest to rozszerzenie pierwszych wrsji grier z serii tetris gdzie nie wprowadzono jeszcze grawitacyjnego spadania klocków.
Gra została napisana w języku MATLAB i opiera się na toolboxie Guide, który pozwolił uzyskać ciekawy interfejs użytkownika.
Do opadania klocków został dodatkowo użyty obiekt klasy timer z przestrzeni roboczej MATLABa.
 
Rozszerzenia polegają mniędzy innymi na:
- generowaniu losowych klocków o wielkościach z przedziału od 4 do 10 w zależności od preferencji użytkownika
- wprowadzenie 2 trybów gry -> klasycznego i bonusowego, gdzie zostały dodane specjalne klocki mające na celu urozmaicenie
rozgrywki.

Autor: Bartosz Nowak