function [ current_block, current_possition ] = block_fall(current_block, current_possition, game_difficulty_lvl )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

%stworzenie nowego bloku

if(isempty(current_block))
    current_block=rand_tetris_block(game_difficulty_lvl);
    [row, col]=find(current_block==1);
    row=row+1;
    col=col+(size(current_possition,2)/2-(ceil(size(current_block,2)/2)));
    current_possition=index2matrix(row, col, 1, current_possition);
else
%odnalezienie i opuszczenie juz istniejącego
    [row, col]=find(current_possition==1);
    current_possition=index2matrix(row, col, 0, current_possition);
    row=row+1;
    current_possition=index2matrix(row, col, 1, current_possition);
end


end