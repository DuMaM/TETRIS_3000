function [ final_array ] = add2matrix_block( moving_array, block_array, row_pos, col_pos )
%pozwala na dodanie do siebie 2 r�nej wielko�ci macierzy

final_array = zeros(max(size(moving_array), max(size(block_array)))); 
final_array(1:size(moving_array, 1), 1:size(moving_array, 2)) = moving_array;

start_row=row_pos;
end_row=row_pos-1+size(block_array, 1);

start_col=col_pos;
end_col=col_pos-1+size(block_array, 2);

final_array(start_row:end_row, start_col:end_col) = ...
final_array(start_row:end_row, start_col:end_col) + block_array;

end

