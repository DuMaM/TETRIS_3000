function [block_array] = rand_tetris_block( dificulty_lvl, bonus )

%losowanie bonusowaego klocka
block_array=[];
if bonus
    tmp=(rand<(dificulty_lvl/100));
    if tmp
        block_array=7;
    end
end

if isempty(block_array)
    %losowanie wielkocsi opadajcego klocka
    while inf
        %dziwne zachowanie randn bo losuje zmienne z poza przedzialu <0;1)
        tmp=randn(1);
        if tmp <1 && tmp >=0
            break;
        end
    end
    rand_block_size=ceil(tmp*dificulty_lvl);
    
    color=ceil(rand*6);
    
    block_array=zeros(rand_block_size);
    block_array(ceil(rand_block_size/2),ceil(rand_block_size/2))=color;
    
    %budowa klocka
    for i=2:rand_block_size
        while inf
            [row, col]=find(block_array>=1);
            n_place=ceil(rand*(i-1));
            n_position=ceil(rand*4);
            %okreslenie strony po ktorej bedzie dopisana wartosc
            %1-2 lewa-prawa, 3-4 gora-dol
            switch n_position
                case 1
                    new_row=row(n_place)+1;
                case 2
                    new_row=row(n_place)-1;
                case 3
                    new_col=col(n_place)+1;
                case 4
                    new_col=col(n_place)-1;
            end
            %umieszczenie elementu w odpowiednimi miejscu w tymczasowej tablicy
            if(n_position>=3)
                if(new_col>=1 && new_col <=rand_block_size)
                    if(block_array(row(n_place), new_col)==0);
                        block_array(row(n_place), new_col)=color;
                        break;
                    end
                end
            elseif n_position<3
                if(new_row>=1 && new_row<=rand_block_size)
                    if(block_array(new_row, col(n_place))==0);
                        block_array(new_row, col(n_place))=color;
                        break;
                    end
                end
            end
        end
    end
    
    %obcinanie niepotrzebnych zer w wygenerowanym bloku
    if(size(block_array)>1)
        [row, col]=find(block_array>=1);
        n_min=min([row col]);
        row=row-(n_min(1)-1);
        col=col-(n_min(2)-1);
        block_array=index2matrix(row,col, color, zeros(max([row, col])));
    end
end
end




