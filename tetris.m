function varargout = tetris(varargin)
% TETRIS MATLAB code for tetris.fig
%      TETRIS, by itself, creates a new TETRIS or raises the existing
%      singleton*.
%
%      H = TETRIS returns the handle to a new TETRIS or the handle to
%      the existing singleton*.
%
%      TETRIS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TETRIS.M with the given input arguments.
%
%      TETRIS('Property','Value',...) creates a new TETRIS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before tetris_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to tetris_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help tetris

% Last Modified by GUIDE v2.5 30-Oct-2014 00:38:02

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @tetris_OpeningFcn, ...
    'gui_OutputFcn',  @tetris_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

function tetris_OpeningFcn(hObject, eventdata, handles, varargin)

global game_status;             %flaga sygnalizuj�ca stan gry
global game_speed;              %pr�dko�� gry
global game_board;              %plansza gry
global game_difficulty_lvl;     %poziom trudno�ci
global current_block;           %obecnie spadaj�cy klocek
global row_pos;                 %obecny wiersz tablicy z kolckiem
global col_pos;                 %obecna kolumna tablicy z kolckiem
global game_title;              %flaga przechowuj�ca warto�� czy zosta� wy�wietlony obraz tytu�owy
global board_size;              %rozmiar planszy
global next_block;              %tablica przechowuj�ca nast�pny wy�wietlany klocek
global col;                     %wektor przechowuj�cy kolumny w jakich zawiera si� obecny klocek
global row;                     %wektor przechowuj�cy wiersze w jakich zawiera si� obecny klocek
global next_screen;             %tablica na kt�rej jest wy�wietlany klocek
global level_scale;             %sta�a okre�laj�ca ile wierszy trzeba usun�� w grze by osi�gn�� nast. poziom
global color_map;               %mapa kolor�w klock�w
global bonus;                   %zmienna wskazuj�ca na to czy gra korzysta z trybu bonusowego                   

%ustawienia gry
bonus=0;
level_scale=5;
col=[];
row=[];
next_screen=[];
next_block=[];
game_status=0;
game_title=1;
game_speed=1;
current_block=[];
game_board=[];
game_difficulty_lvl=6;
board_size=[12+game_difficulty_lvl 6+game_difficulty_lvl];
row_pos=2;
col_pos=ceil(board_size(2)/2);

%ustawianie palety barw
color_map=ones(255,3);
%biel
color_map(1,:)=[1 1 1];
%czer�
color_map(255,:)=[0 0 0];
%niebieski
color_map(2,:)=[0 0.635 0.91];
%zielony
color_map(3,:)=[0.710 0.902 0.118];
%czerwony
color_map(4,:)=[0.886 0.07 0.114];
%��ty
color_map(5,:)=[0.976 0.925 0];
%pomara�cz
color_map(6,:)=[1 0.658 0.153];
%fiolet
color_map(7,:)=[0.482 0.216 0.482];
%r�
color_map(8,:)=[1 0 1];
%turkus
color_map(9,:)=[0 1 1];


%ustawianie zegara
handles.t = timer('Name', 'Delay');
set(handles.t,'ExecutionMode','fixedrate');
set(handles.t,'Period',game_speed);
set(handles.t, 'TimerFcn',{@block_fall_matrix, hObject});
set(handles.t, 'StartFcn',{@start_timer, hObject});

%wy�wietlanie obraz�w tytu�owych
loading_screen=imread('Game_Name.png');
axes(handles.MAIN_PANEL);
handles.image(1,1)=imshow(loading_screen,  'InitialMagnification','fit');

next_block_loading=imread('Next_Blok_Title.PNG');
axes(handles.NEXT_BLOCK_PANEL);
handles.image(2,1)=imshow(next_block_loading,  'InitialMagnification','fit');

set(handles.HARDNES_BOX,'Value', 3);

handles.output = hObject;
guidata(hObject, handles);

function varargout = tetris_OutputFcn(hObject, eventdata, handles)
varargout{1} = handles.output;

function tetris_DeleteFcn(hObject, eventdata, handles)
if strcmp(get(handles.t, 'Running'), 'on')
    stop(handles.t)
end
handles=rmfield(handles,'t');
guidata(hObject, handles);
delete(hObject);

function START_BUTTON_Callback(hObject, eventdata, handles)
%redelkaracja zmiennych globalnych
global game_status;
global game_board;
global game_difficulty_lvl;
global current_block;
global col_pos;
global row_pos;
global game_speed;
global next_block;
global board_size;
global game_title;
global level_scale;
global bonus;

%zmiana opisu buttona
button_dis=get(hObject,'String');
switch button_dis
    case 'Start Game'
        %konfiguracja gry
        game_difficulty_lvl = 3+get(handles.HARDNES_BOX,'Value');
        game_status=1;
        current_block=[];
        set(handles.POINTS_VALUE,'String', '0');
        set(handles.LEVEL_VALUE,'String', '1');
        game_speed=1;
        set(handles.t,'Period',game_speed);
        
        %okre�lenie trybu gry
        if get(handles.MODE_MARK,'Value')
            level_scale=1;
        else
            level_scale=5;
        end
        
        %okre�lenie trybu gry
        if get(handles.BONUS_MARK,'Value')
            bonus=1;
        else
            bonus=0;
        end
        
        %przygotowywanie planszy
        set(hObject, 'String' , 'Stop Game');
        board_size=[12+game_difficulty_lvl 6+game_difficulty_lvl];
        game_board=zeros(board_size(1),board_size(2));
        game_board(:,1)=255;
        game_board(1,:)=255;
        game_board(:,board_size(2))=255;
        game_board(board_size(1),:)=255;
        
        %start zegara
        if strcmp(get(handles.t, 'Running'), 'off')
            start(handles.t);
        end
        
    case 'Stop Game'
        game_status=0;
        next_block=[];
        row_pos=2;
        col_pos=ceil(board_size(2)/2);
        current_block=[];
        game_title=1;
        
        loading_screen=imread('Game_Name.png');
        axes(handles.MAIN_PANEL);
        imshow(loading_screen);
        
        next_block_loading=imread('Next_Blok_Title.PNG');
        axes(handles.NEXT_BLOCK_PANEL);
        imshow(next_block_loading);
        
        %zatrzymanie timera
        if strcmp(get(handles.t, 'Running'), 'on')
            stop(handles.t);
        end
        set(hObject, 'String' , 'Start Game');
end
% aktualizacja
guidata(hObject, handles);

function HARDNES_BOX_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
guidata(hObject, handles);

function BONUS_MARK_Callback(hObject, eventdata, handles)

function MODE_MARK_Callback(hObject, eventdata, handles)

function tetris_KeyPressFcn(hObject, eventdata, handles)

global current_block;
global game_board;
global row_pos;
global col_pos;

global row;
global col;

if (false==isempty(current_block))
    movment_flag=[];
    press_button=eventdata.Key;
    
    game_board=add2matrix_block(game_board, -1*current_block, row_pos, col_pos);
    
    switch (press_button)
        case 'space'
            row_pos=row_pos+1;
        case 'uparrow'
            current_block  = rotate_block( current_block, 'right');
        case 'downarrow'
            current_block  = rotate_block( current_block, 'left');
        case 'leftarrow'
            col_pos=col_pos-1;
        case 'rightarrow'
            col_pos=col_pos+1;
    end
    [row, col]=find(current_block>=1);
    row=row-1+row_pos;
    col=col-1+col_pos;
    
    for i=1:nnz(current_block)
        result=game_board(row(i), col(i));
        if(result==0)
            movment_flag=1;
        else
            movment_flag=0;
            break;
        end
    end
    
    if movment_flag==1
        game_board=add2matrix_block(game_board, current_block, row_pos, col_pos);
        handles.image=findall(0,'type','image');
        set(handles.image(1,1), 'CData', uint8(game_board));
    else
        switch (press_button)
            case 'space'
                row_pos=row_pos-1;
            case 'uparrow'
                current_block  = rotate_block( current_block, 'left');
            case 'downarrow'
                current_block  = rotate_block( current_block, 'right');
            case 'leftarrow'
                col_pos=col_pos+1;
            case 'rightarrow'
                col_pos=col_pos-1;
        end
        game_board=add2matrix_block(game_board, current_block, row_pos, col_pos);
        [row, col]=find(current_block>=1);
        row=row-1+row_pos;
        col=col-1+col_pos;
        
    end
    guidata(hObject, handles);
    
end

function start_timer(hObject,eventdata,hfigure)

global game_difficulty_lvl;
global next_block;
global game_board;
global game_title;
global next_screen;
global color_map;
global bonus;

handles = guidata(hfigure);

if game_title==1
    game_title=0;
    next_block=rand_tetris_block(game_difficulty_lvl, bonus);
    next_screen=zeros(game_difficulty_lvl+4);
    y_pos=ceil((game_difficulty_lvl+4)/2-(size(next_block,1))/2);
    x_pos=ceil((game_difficulty_lvl+4)/2-(size(next_block,2))/2);
    next_screen=add2matrix_block(next_screen, next_block, y_pos ,  x_pos);
    axes(handles.NEXT_BLOCK_PANEL);
    handles.image(2,1)=imshow(uint8(next_screen), color_map, 'InitialMagnification','fit');
    axes(handles.MAIN_PANEL)
    handles.image(1,1)=imshow(uint8(game_board), color_map, 'InitialMagnification','fit' );
end

function block_fall_matrix(hObject,eventdata,hfigure)

global game_difficulty_lvl;
global next_block;
global current_block;
global game_status;
global game_board;
global row_pos;
global col_pos;
global col;
global row;
global next_screen;
global board_size;
global game_speed;
global level_scale;
global bonus;

movment_flag=[];
handles = guidata(hfigure);

if game_status==1
    if(isempty(current_block)==1)
        current_block=next_block;
        
        %zapisanie nowego klocka do panelu->next_block
        next_block=rand_tetris_block(game_difficulty_lvl, bonus);
        next_screen=zeros(game_difficulty_lvl+4);
        y_pos=ceil((game_difficulty_lvl+4)/2-(size(next_block,1))/2);
        x_pos=ceil((game_difficulty_lvl+4)/2-(size(next_block,2))/2);
        next_screen=add2matrix_block(next_screen, next_block, y_pos ,  x_pos);
        
        %pozycjonowanie klocka na planszy
        [row, col]=find(current_block>=1);
        row=row-1+row_pos;
        col=col-1+col_pos;
        for i=1:nnz(current_block)
            
            %warunek na zako�czenie gry
            if(game_board(row(i), col(i))==0)
                movment_flag=1;
            else
                %wpisanie resztek klocka w plansze
                current_block(1,:)=[];
                while 1
                    if isempty(current_block)
                        break;
                    end
                    [row, col]=find(current_block>=1);
                    row=row-1+row_pos;
                    col=col-1+col_pos;
                    for k=1:nnz(current_block)
                        if(game_board(row(k), col(k))>0)
                            current_block(1,:)=[];
                        else
                            if k==nnz(current_block)
                                game_board = add2matrix_block( game_board, current_block, row_pos, col_pos);
                                current_block=[];
                            end
                        end
                    end
                end
                
                set(handles.START_BUTTON, 'String' , 'Start Game');
                game_status=0;
                if strcmp(get(handles.t, 'Running'), 'on')
                    stop(handles.t);
                end
                set(handles.START_BUTTON, 'String' , 'Start Game');
                movment_flag=0;
                break;
            end
        end
        if movment_flag==1
            game_board = add2matrix_block( game_board, current_block, row_pos, col_pos);
        end
    else
        %odnalezienie i opuszczenie juz istniej�cego
        game_board=add2matrix_block(game_board, -1*current_block, row_pos, col_pos);
        row=row+1;
        for i=1:nnz(current_block)
            if(game_board(row(i), col(i))==0)
                movment_flag=1;
            else
                movment_flag=0;
                break;
            end
        end
        
        if movment_flag==1
            %zapisanie na nowej pozycji
            row_pos=row_pos+1;
            game_board=add2matrix_block(game_board, current_block, row_pos, col_pos);
        else
            %zapisanie po�o�enie klocka
            game_board=add2matrix_block(game_board, current_block, row_pos, col_pos);
            
            %wybuch bomby
            if max(max(current_block))==7
                bomba=ones(5,5);
                bomba(1,1)=0;
                bomba(1,5)=0;
                bomba(5,5)=0;
                bomba(5,1)=0;
                for w=1:5
                    for k=1:5
                        if(bomba(w,k)==1)
                            if((row_pos-3+w<board_size(1)) && (row_pos-3+w>1) && (col_pos-3+k>1) && (col_pos-3+k<board_size(2)))
                                game_board(row_pos+w-3,col_pos-3+k)=0;
                            end
                        end
                    end
                end
            end
            
            %usuwanie wiersza
            block_height=size(current_block,1);
            compare_block=ones(1,board_size(2)-2);
            row_2_delete=zeros(board_size(1),1);
            for i=row_pos:(row_pos-1+block_height)
                if(game_board(i, 2:board_size(2)-1)>=compare_block)
                    row_2_delete(i)=i;
                end
            end
            rowdelete=nonzeros(row_2_delete);
            
            %opuszczczanie planszy
            if isempty(rowdelete)==0
                emptyline=zeros(1,board_size(2));
                emptyline(1)=255;
                emptyline(board_size(2))=255;
                
                tmp=rowdelete;
                %usuni�cie wiersza z tablicy
                for i=1:nnz(tmp)
                    game_board(tmp(i), :)=[];
                    tmp=tmp-1;
                end
                %wrzucenie pustego na pocz�tek
                for i=1:nnz(row_2_delete)
                    game_board=cat(1 ,game_board(1,:),emptyline, game_board(2:size(game_board,1),:));
                end
                
                %naliczanie punkt�w
                points=get(handles.POINTS_VALUE, 'String');
                points_value=str2num(points);
                points_value=points_value+((board_size(2)-2)*size(rowdelete,1));
                points=num2str(points_value);
                set(handles.POINTS_VALUE,'String', points);
                
                %ustawianie poziomu
                if game_speed>0.1
                    if rem(points_value, level_scale*(board_size(2)-2))==0
                        levels=get(handles.LEVEL_VALUE, 'String');
                        levels_value=str2num(levels);
                        levels_value=levels_value+1;
                        levels=num2str(levels_value);
                        set(handles.LEVEL_VALUE,'String', levels);
                        game_speed=round((1000/levels_value))/1000;
                        stop(handles.t);
                        set(handles.t,'Period',game_speed);
                        start(handles.t);
                    end
                end
            end
            
            %resetowanie klocka, kt�re wymusi stwozenie nast. bloku
            row_pos=2;
            col_pos=ceil(board_size(2)/2);
            current_block=[];
        end
    end

    handles.image=findall(0,'type','image');
    set(handles.image(2,1), 'CData', uint8(next_screen));
    set(handles.image(1,1), 'CData', uint8(game_board));
    guidata(hfigure, handles);
end
